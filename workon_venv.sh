### Instala programas para el manejo de entornos virtuales de Python. Se debe ejecutar como
### root. Si se especifica un nombre de usuario, actualiza el archivo .zshrc para ese usuario,
### si no lo modifica para el usuario que ejecuta el script.
###
### Comandos una vez instalados los programas:
###	$ mkvirtualenv venv_name -p python3 -> Crea un nuevo entorno virtual para Python 3.
###	$ workon venv_name -> Activa un entorno virtual existente.
###	$ deactivate -> Desactiva el entorno virtual activo.

#!/bin/bash

USER_NAME=$1
if [[ $USER_NAME != '' ]]; then
	PROFILE_DIR="$HOME/.profile"
else
	PROFILE_DIR="/home/$USER_NAME/.profile"
fi

apt update && apt install python3-pip
pip3 install virtualenv virtualenvwrapper

echo -e "\n# virtualenv and virtualenvwrapper" >> $PROFILE_DIR
echo "export WORKON_HOME=$HOME/.virtualenvs" >> $PROFILE_DIR
echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3" >> $PROFILE_DIR
echo "source /usr/local/bin/virtualenvwrapper.sh" >> $PROFILE_DIR

source $PROFILE_DIR
